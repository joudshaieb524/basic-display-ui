import './styles.css'
import customBtn from '../../components/Button/index'
import {Link} from 'react-router-dom'
import {BsFillGearFill} from 'react-icons/bs'
import {Button} from "reactstrap";

type HomeProps = {
    child?: React.ReactNode
    address?: string
}

const Home = (props: HomeProps) => {

    const experience1BtnHandler = () => {
        console.log('btn1')
    }

    return (
        <div className='Home'>
            <div className='btnsWrapper'>
                
                <Link to="/chapitares" >
                    {/* <BsFillGearFill icon="signal" style={{height:'25px'}}/> */}
                    <Button color='primary' onClick={experience1BtnHandler}>Chapitres/Sous-Chapitres</Button>
                </Link>

                <div className='divider'></div>

                <Link to="/examens" >
                    <Button color='primary' onClick={experience1BtnHandler}>Examens/Analyses</Button>
                </Link>

                <div className='divider'></div>

                <Link to="/results" >
                    <Button color='primary' onClick={experience1BtnHandler}>Results</Button>
                </Link>
            </div>
        </div>
    )
}

export default Home