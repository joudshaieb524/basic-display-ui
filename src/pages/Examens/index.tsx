import React, {useState, useEffect} from "react"
import StrapInput from '../../components/StrapInput/index'
import Spinner from '../../components/Spinner'
import GeneralHelper from '../../helpers/general'
import Papa from "papaparse";
import CustomTable from '../../components/CustomTable'
import CustomRadio from '../../components/CustomRadio'
import testData from './test-output.json'
import './styles.css'

const Examens = () => {

    const [report="", setReport] = useState();
    const [reportArray, setReportArray] = useState<object[]>([]);
    const [fileType, setFileType] = useState("");
    const [fileName, setFileName] = useState("");
    const [reqHeader, setReqHeader] = useState({});
    const [reqBody, setReqBody] = useState("");
    const [showLoader, setShowLoader] = useState(false);
    const [spanError, setSpanError] = useState('');
    const [testReportData, setTestReportData] = useState<any[]>([])
    const [examenChecked, setExamenChecked] = useState(true)

    const sglKeys : Array<string> = ['Code_Chapitre_SGL', 'Libellé_Chapitre_SGL', 'Code_Sous_Chapitre_SGL', 'Libellé_Sous_Chapitre_SGL']
    const generalHelper = new GeneralHelper()

    async function handelFile(val : any) { 
        if (val.length >= 1) {
            let fileInput : any ;
            let file = val[0];
            setFileName(file["name"]);
            localStorage.setItem('reportName', file["name"])
            setFileType(file.type);
            let reader = new FileReader();
            file && reader.readAsText(file);
            reader.onload = async function (e : any) {
                switch (file.type) {
                    case "application/json":
                        // fileInput = JSON.parse(reader.result);
                    break;
    
                    case "text/xml": case "text/plain":
                        fileInput = reader.result;
                    break;

                    case "application/vnd.ms-excel":
                        if (typeof reader.result == 'string') {

                            Papa.parse(reader.result, {
                                complete: function(results) {
                                  let jsonArr: object[] = []
                                  let csvArrays = results.data
                                    csvArrays.forEach((arrayItem : any) => {
                                        const reducer = (res:any , item:any, i: number) => {
                                            res[`${sglKeys[i]}`] = item
                                            return res
                                        }
                                        jsonArr.push(arrayItem.reduce(reducer, {}))
                                        }
                                    ) 
                                    setReportArray(jsonArr)   
                                }
                            })
                        }
                    break;

                }
                // setReport(fileInput);
                // onSubmit(e)
                fakeReportLoad()
            };
        } 
    }

    function onSubmit(e: React.FormEvent<HTMLFormElement>) {
        console.log('on submit...')
        e.preventDefault();
        setShowLoader(true);
    }

    function fakeReportLoad() {
        setShowLoader(true)
        setTestReportData([])
        setTimeout(() => {
            setShowLoader(false)
            setTestReportData(testData['LOINC_Examen'])
        }, 1000)
    }

    const onRadioChange = (displayDataType: string) => {
        switch (displayDataType) {
            case 'examen':
                setTestReportData(testData['LOINC_Examen'])
                setExamenChecked(true)

            break

            case 'analyse':
                setTestReportData(testData['LOINC_Analyse'])
                setExamenChecked(false)
            break
        }   
    }

    return (
        <div>
            <div className="formContainer">
                <form onSubmit={onSubmit}>
                    <div className="row m-0">
                        <div className="col-4 m-auto">
                            <StrapInput
                                id='csvFile'
                                name='csvFile'
                                placeholder='placeholder'
                                type='file'
                                disabledState={false}
                                getValue={(val : any) => {handelFile(val)}}
                            />
                        </div>
                        <div>{showLoader ? (<Spinner/>) : null}</div>
                        <div className="col-2 m-auto mt-3">
                            <span style={{color:'red'}}>{spanError ? 'Request timeout!' : null}</span>
                        </div>
                    </div>
                </form>
            </div>

            {testReportData.length > 0 &&

                <div className="radiosWrapper">
                   <CustomRadio value="examen" checked={examenChecked} onChange={() => onRadioChange('examen')} text=" Loinc Examen" ></CustomRadio>
                   <div className="divider"></div>
                   <CustomRadio value="analyse" checked={!examenChecked} onChange={() => onRadioChange('analyse')} text=" Loinc Analyse" ></CustomRadio>
                </div>

            }
            <div className="ResultTable">
                {testReportData.length > 0 &&
                    <div>

                        <CustomTable
                            tableData = {testReportData}
                            fileName = {fileName}
                        ></CustomTable>
                    </div>

                }
            </div>
        </div>
    )
}

export default Examens