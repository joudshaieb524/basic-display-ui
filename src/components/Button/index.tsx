import React from "react"
import {Button} from "reactstrap";

type ButtonProps = {
    btnName: string,
    id: string,
    handleClick: (e: React.MouseEvent<HTMLButtonElement>, id: number) => void
}

const customBtn = (props: ButtonProps) => {
    return (
        <div>
            <Button id={props.id} onClick={(event) => props.handleClick(event, 1)}>{props.btnName}</Button>
        </div>
    )
}

export default customBtn