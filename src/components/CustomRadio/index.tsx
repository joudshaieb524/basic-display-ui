type RadioProps = {
    value: string
    checked: boolean
    onChange: any
    text: string
}

const CustomRadio = ({value, checked, onChange, text} : RadioProps) => {
    return (
        <div>
            <div className="radio">
                <label>
                    <input
                        type="radio"
                        value={value}
                        checked={checked}
                        onChange={onChange}
                    />
                    {text}
                </label>
            </div>
        </div>
         
    );
}

export default CustomRadio