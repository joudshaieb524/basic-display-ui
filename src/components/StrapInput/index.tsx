import React, { useRef } from "react";
import { Input, Label } from "reactstrap";

type StrapInputProps = {
    id: string;
    name: string;
    label?: string;
    value?: string;
    placeholder: string;
    type: any;
    disabledState : boolean;
    getValue?: any
}

const StrapInput = ({id, name, label, type, placeholder, value, disabledState, getValue}: StrapInputProps) => {

    function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
        type === "file"
          ? getValue(event.target.files)
          : getValue(event.target.value);
      }

    return (
        <div className="mb-4">
            <Label for={id}>{label}</Label>
            <input
                className="form-control"
                id={id}
                name={name}
                type={type}
                placeholder={placeholder}
                onChange={handleChange}
                value={value}
                accept=".txt,.csv"
                disabled={disabledState}
            />
      </div>
    )
};

export default StrapInput;
