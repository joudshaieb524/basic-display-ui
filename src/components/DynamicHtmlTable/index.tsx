import "./styles.css"
import React, {useState, useEffect} from "react"
import GeneralHelper from '../../helpers/general'

type TableProps = {
    tableData: any[]
    tableHtmlClass: string
    conditionalColorProperty: string
    
}

const DynamicHtmlTable = ({tableData, tableHtmlClass, conditionalColorProperty}: TableProps) => {
    const generalHelper = new GeneralHelper()

    const conditionalTableRowColor = (rowId: number) => rowId % 2 === 0 ? 'white' : 'rgb(231, 225, 218)'

    const getKeys = (inputArray: any[]) => Object.keys(inputArray[0]);

    const getHeader = (keysArray: any[]) => keysArray.map((key) => <th id="tableHeader" key={key}>{key.toUpperCase()}</th>)
    
    const RenderRow = (props: any) => {
        return props.keys.map((key : any)=>{
            return <td style={{background: `${conditionalTableRowColor(props.data[`${conditionalColorProperty}`])}`}} key={props.data[key]}>
                {props.data[key]}
            </td>
        })
    }

    const getRowsData = (tableArray: any[], keysArray: any[]) => tableArray.map((row, index) => <tr key={index}><RenderRow key={index} data={row} keys={keysArray}/></tr>)

    return (
        <div>
            <table className={tableHtmlClass}>
                <thead>
                    <tr>{getHeader(generalHelper.removeItemFromArray(getKeys(tableData), 'incId'))}</tr>
                </thead>
                <tbody>
                    {getRowsData(tableData, generalHelper.removeItemFromArray(getKeys(tableData), 'incId'))}
                </tbody>
            </table>
        </div>
    )
}

export default DynamicHtmlTable