import "./styles.css"
import React, {useState, useEffect} from "react"
import GeneralHelper from '../../helpers/general'
import {Button} from "reactstrap";
import DynamicHtmlTable from '../DynamicHtmlTable'

type TableProps = {
    tableData: any[]
    fileName: string
}

const CustomTable = ({tableData, fileName}: TableProps) => {

    const [exportedRows, setExportedRows] = useState<any[]>([]);
    const [exportedBtnDisable, setExportedBtnDisable] = useState(false);
    const [testTable, setTestTable] = useState(true);
    const generalHelper = new GeneralHelper()

    useEffect(() => {
        let tableDataWithIncrementalId = generalHelper.giveIncrementalIdToArrayObjects(tableData)
        setExportedRows(tableDataWithIncrementalId)
    }, [])

    useEffect(() => {
        conditionalExportBtnDisable()
    }, [exportedRows])

    const conditionalTableRowColor = (rowId: number) => rowId % 2 === 0 ? 'white' : 'rgb(231, 225, 218)'

    const conditionalExportBtnDisable = () => exportedRows.length === 0 ? setExportedBtnDisable(true) : setExportedBtnDisable(false)
    
    const getKeys = (inputArray: any[]) => Object.keys(inputArray[0]);

    const getHeader = (keysArray: any[]) => keysArray.map((key, index) => <th  id="tableHeader" key={key}>{key.toUpperCase()}</th>)
    
    const RenderRow = (props: any) => {
        return props.keys.map((key : any, index : any)=>{
            if (key === 'Required') {
                return <td style={{background: `${conditionalTableRowColor(props.data.Code_chapitre_SGL)}`}} key={props.data[key]}>
                    <input 
                        type='checkbox'
                        id={`${props.data.incId}`}
                        defaultChecked={props.data.Required}
                        onClick={() => {handleCheckBoxChange(props.data.incId)}}>
                    </input>
                </td>
            } else {
                return <td style={{background: `${conditionalTableRowColor(props.data.Code_chapitre_SGL)}`}} key={props.data[key]}>
                    {props.data[key]}
                </td>
            }
        })
    }

    const getRowsData = (tableArray: any[], keysArray: any[]) => tableArray.map((row, index) => <tr key={index}><RenderRow key={index} data={row} keys={keysArray}/></tr>)

    const handleCsvExport = () => {
        exportedRows.sort((a,b) => (a.Code_chapitre_SGL > b.Code_chapitre_SGL) ? 1 : ((b.Code_chapitre_SGL > a.Code_chapitre_SGL) ? -1 : 0))
        let csvData = generalHelper.convertJson2CsvStrings(exportedRows)
        var BOM = "\uFEFF"
        var csvLatinContent : any = BOM + csvData;
        generalHelper.downloadCSVFile(csvLatinContent, `${fileName}-result.csv`, 'text/csv;charset=windows-1252')
    }

    const handleCheckBoxChange = (rowId: number) => {
        let activeRows = []
        let checkBox = document.getElementById(`${rowId}`) as HTMLInputElement;
        let selectedRow = generalHelper.getItemFromArrayByPropertyVal(tableData, 'incId', rowId)
        activeRows.push(selectedRow)

        if (checkBox !== null && checkBox.checked == true) {
            exportedRows.push(selectedRow)
            setExportedRows(exportedRows)
            selectedRow['Required'] = true  
        } else {
            let subtractedArray = generalHelper.removeItemFromArrayByPropertyValMap(exportedRows, 'incId', rowId)
            setExportedRows(subtractedArray)
            selectedRow['Required'] = false
        }
    }

    return (

        <div>
            {testTable 
            ? <div className='ssss'>
                <DynamicHtmlTable 
                    tableData={tableData} 
                    tableHtmlClass="table table-secondary table-sm table-bordered" 
                    conditionalColorProperty="Code_chapitre_SGL" 
                ></DynamicHtmlTable>
              </div>
            : <div> 
                    <div>
                       <Button color="primary" id="exportBtn" disabled={exportedBtnDisable} onClick={handleCsvExport}>export to CSV</Button>
                    </div>
                    <table className="table table-secondary table-sm table-bordered">
                        <thead>
                            <tr>{getHeader(generalHelper.removeItemFromArray(getKeys(tableData), 'incId'))}</tr>
                        </thead>
                        <tbody>
                            {getRowsData(tableData, generalHelper.removeItemFromArray(getKeys(tableData), 'incId'))}
                        </tbody>
                    </table>
                </div>
            }
        </div>
    )
}

export default CustomTable