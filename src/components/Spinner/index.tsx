import spinner from './spinner.gif';

const Spinner = () => {
  return (
    <div style={{backgroundColor : 'rgba(245, 245, 245, 0)'}}>
      <img
        src={spinner}
        style={{ width: '200px', margin: 'auto', display: 'block' }}
        alt="Loading..."
      />
    </div>
  );
};

export default Spinner;