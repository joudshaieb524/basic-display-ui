export default class GeneralHelper {

    public async fetchWithTimeout(resource : any, options : any) {
        const { timeout = 30000 } = options;
        
        // AbortController stops fectch at will (for each request)
        const controller = new AbortController();

        // start timing and abort req after timeout
        const id = setTimeout(() => controller.abort(), timeout);
      
        const response = await fetch(resource, {
          ...options,
          signal: controller.signal  
        });
        clearTimeout(id);
      
        return response;
    }

    public createFileRequest = async (fileData: any, fileType: string) => {
        let header : object = {};
        let body : string = "";

        switch (fileType) {
            case "application/json":
            case "application/vnd.ms-excel":
                header = {"Content-type": "application/json; charset=UTF-8"};
                body = JSON.stringify(fileData);
            break;

            case "text/xml":
                header = {"Content-type": "application/xml; charset=UTF-8"};
                body = fileData;
            break;

            case "text/plain":
                header = {"Content-type": "text/plain; charset=UTF-8"}; 
                body = fileData;
            break;

            case "application/pdf":
            case "image/png":
            case "image/jpeg":
                header = {"Content-type": "text/plain; charset=UTF-8"};
                body = fileData;
            break;

            default: break
        }

        return {
            reqHeader: header,
            reqBody: body
        }
    }

    public downloadCSVFile = (data : any, fileName : string, fileType: string) => {
		const element = document.createElement("a")
		const file = new Blob([data], {type: fileType})
		element.href = URL.createObjectURL(file)
		element.download = fileName
		document.body.appendChild(element)
		element.click()
	}

    public convertJson2CsvStrings = (jsonArray : object[]) => {
        let strData : string = ""
        let headers = Object.keys(jsonArray[0])
        strData = headers + "\n"

        jsonArray.forEach((item, index) => {
            let line = Object.values(item)
            strData += line + "\n"
        })

        let semiColonStr = strData.replace(/,/g, ';')
        return semiColonStr
    }

    public getItemFromArrayByPropertyVal = (arrayInput : any[], itemPropertyKey : string, itemPropertyValue : any) => {
        return arrayInput.filter((item : any) => item[`${itemPropertyKey}`] === itemPropertyValue)[0] 
    }

    public removeItemFromArrayByPropertyValFilter = (arrayInput : any[], itemPropertyKey : string, itemPropertyValue : any) => {
        return arrayInput.filter((item : any) => item[`${itemPropertyKey}`] !== itemPropertyValue) 
    }

    public removeItemFromArrayByPropertyValMap = (arrayInput : any[], itemPropertyKey : string, itemPropertyValue : any) => {
        let subtractedArray : any[] = []
        arrayInput.map((item : any) => item[`${itemPropertyKey}`] !== itemPropertyValue ? subtractedArray.push(item) : null)
        return subtractedArray
    }

    public checkItemInArrayByPropertyValue = (arrayInput : any[], itemPropertyKey : string, itemPropertyValue : any) => {
        let itemToReturn : string | number | object = {}
        let found = false

        arrayInput.map((item : any) => {
            if (item[`${itemPropertyKey}`] === itemPropertyValue) {
                found = true
                itemToReturn = item
            }
        })

        if (found) {
            return itemToReturn
        } else {
            return false
        }
    }

    public getRequiredDataFromArrayByVal = (arrayInput : any[], itemPropertyKey : string, itemPropertyValue : any) => {
        let outputArray : any[] = []
        console.log('array input : ', arrayInput)
        arrayInput.map((item : any) => {
            console.log('item[`${itemPropertyKey}`] : ', item[`${itemPropertyKey}`])
            if (item[`${itemPropertyKey}`] === itemPropertyValue) {
                outputArray.push(item)
            }
        })

        console.log('array output : ', outputArray)
        return outputArray
    }

    public removeFieldFromObjectsArray = (arrayInput : any[], fieldToDelete : string) => {
        arrayInput.forEach((item : any) => {
            delete item[`${fieldToDelete}`]
        })

        return arrayInput
    }

    public giveIncrementalIdToArrayObjects = (arrayInput: any[]) => {
        let itemId = 1
        arrayInput.map((item : any) => {
            item['incId'] = itemId
            itemId += 1
        }, arrayInput)

        return arrayInput
    }  
    
    public removeItemFromArray = (arrayInput: any[], itemToRemove: any) => {
        return arrayInput.filter((item: any) => item != itemToRemove)
    }
}