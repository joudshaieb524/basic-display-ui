import React from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import Home from './pages/Home'
import Chapitares from './pages/Chapitares'
import Examens from './pages/Examens'
import Results from './pages/Results'

function App() {
  document.title = "Segur de la Sante"

  return (
    <Router>
      <div className="App">
        <div className="App-logo-header"></div>
        <Routes>
          <Route path='/' element={<Home /*child={<Status status='loading'/>}*/ address={'dsds'}/>}></Route>
          <Route path='/chapitares' element={<Chapitares />}></Route>
          <Route path='/examens' element={<Examens />}></Route>
          <Route path='/results' element={<Results />}></Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
